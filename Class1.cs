﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Formula
    {
        public static double LibOne(double x)
        {
            return x = 1 / (x + 5) * Math.Sin(x);

        }
        public static void LibTwo(double a, double b, double n)
        {
            double dx;
            dx = (b - a) / (n - 1);
            double i;
             
            for (i = a; i <= b; i += dx) 
            {
                Console.WriteLine("x = {0}", "f = {1}", i, LibOne(i));
            }

        }
    }
}
